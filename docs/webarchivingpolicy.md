# Web Archive Program Policy

## Originator

Washington University Archives, Special Collections Management

## Applicability

This policy applies to all Libraries staff involved in the development of web archive collections.  This includes the use of web archiving technology in the acquisition of web content for the Department of Special Collections and related University Libraries initiatives as outlined in the collecting priorities section below.

## Purpose

This policy addresses Washington University Libraries' approach to creating and maintaining web archive collections.  The Libraries has taken responsibility for the acquisition, description, and access of archived web content.  The policy establishes the scope of collecting priorities and outlines basic procedures related to the development and management of web archive collections.  It represents general practices and is written with the understanding that web archiving requires flexibility and openness to different approaches as the web changes over time.

## Definitions

___

| Term | Definition |
| ------------- | ------------- |
| Web archiving | Web archiving is a series of steps that work together for an end goal of preserving website functionality as it looked on the day it was archived.  It uses different technologies to capture, store, and replay websites. |
| Archive-It | Archive-It is the Libraries’ primary web archiving tool and is used to capture, preserve and serve web content.  Archive-It is maintained by Internet Archive, which provides its web archiving solution on a subscription basis.   This is the collection [homepage](https://archive-it.org/home/wustl/). |
| Crawler | A crawler is a robot/bot that goes out to the live web and gathers up all of the source material that make up a web page.   This includes all of the images, text, javascript (dynamic content), and css (formatting and style).   Crawling technology employed by Archive-It includes Heritrix, Umbra and Brozzler. |
| WARC | The crawler stores all captured web content in a web archive file format (WARC IS0 28500).  It is the standard preservation format used by Archive-It, and it can be downloaded and stored locally.   It requires special replay technology to view its contents. |
| Wayback | The Archive-It replay mechanism is called the Wayback Machine.  It provides the ability to view the contents of a WARC file format. |
| Capture | Capture refers to the act of the crawler to gather web content at a specific point in time.   It also refers to web content that was gathered by a crawler. |
| Collection | A group of archived web documents curated around a common theme, topic, or domain. |
| Seed | Seed refers to the URL used as the starting point for crawlers, as well as an access point to archived content. All seeds in a single collection will be de-duplicated and only unique content will be captured on subsequent crawls. |
| Document | Any file with a unique URL - html, image, PDF, video, etc.  These are designated separately from seeds, although seeds can be documents as well. |
| Scope | A set of rules and boundaries that can be applied at the Collection or Seed level to direct the crawler to exclude or include web content.  Web content is either in or out of scope of the seed URL. |
| Robots.txt | Robots exclusion standard is a tool used by webmasters to direct a web crawler not to crawl all or specified parts of a website.  Robots.txt can be ignored by Archive-It crawlers, and the website can be captured. |

## Policy Details

___

Web archiving captures content that is broad in scope, diverse in format, and susceptible to change.  It is necessary to prioritize web archive collecting efforts to properly—and flexibly—scope individual collection priorities to preserve essential web content, while being mindful of the ephemeral nature of the web, data budget constraints, and technological limitations.   Primary web archiving objectives are:

- to capture websites as completely as possible to preserve the functionality of and accessibility to the original content.

- to document Washington University’s web presence including the activities of administration, faculty, students, and staff.

- to serve as a resource (and tool) for projects relevant to the mission of the Libraries that support Special Collections, preservation, research, teaching, and learning.

## Procedures

___

The Digital Processing Archivist works with staff members to evaluate web content for inclusion in the web archive.  Requests are balanced against collecting priorities, available data budget, and technological requirements.  If requests meet these criteria, they may be included in the web archive.  If they do not, the Digital Processing Archivist can advise on alternatives for web archiving the content.

### Collecting Priorities

The goal of prioritization is to control the growth of the web archive to ensure that data is responsibly managed to capture the highest priority web content.   Priorities are listed from highest to lowest.

#### Washington University Websites Collection

>The highest priority and core of the web archive, [WashU Websites Collection](https://archive-it.org/collections/4726) serves as a dedicated arm of University Archives to document historically significant websites related to Washington University.  The primary focus is the university [domain](https://wustl.edu/) and all subdomains encompassing the activities of administration, faculty, students, and staff.  This also includes a selection of social media accounts, student organization pages, and university related events or initiatives that fall within the collecting scope of University Archives.

#### Collecting initiatives within Special Collections

> The focus is on acquiring web content that complements existing collections in the holdings of the Julian Edison Department of Special Collections.  These websites are captured at the request of curatorial staff.  Examples include the [Harley Hammerman Collection on Eugene O'Neill](https://archive-it.org/collections/11063), [ACLU-MO Records](https://archive-it.org/collections/9933), and [St. Louis LGBTQ+ Research Collection](https://archive-it.org/collections/7608)

#### Current events

> This priority focuses on web content documenting Wash U community response to significant historical events.  Examples include [Covid-19](https://archive-it.org/collections/13766), [Documenting Ferguson](https://archive-it.org/collections/4726?fc=meta_Subject%3ADocumenting+Ferguson), and [U.S. Presidential Debates](https://archive-it.org/collections/6327).  These events typically require a rapid response to collect frequent updates and ephemeral content.

#### Libraries-produced web content and related collecting priorities

> This priority focuses on using web archiving technology as a tool to capture and preserve web content produced by or in collaboration with Libraries staff.  This can include online exhibits, news articles, and [research projects](https://archive-it.org/collections/7702). By default, web content preserved elsewhere (institutional repository, digital library) is excluded from capture.  It may also include web content that complements general collecting initiatives from across the Libraries.

### Data Budget

The Libraries’ annual Archive-It budget is limited and is renewed each February.  Requests are evaluated according to the available data budget, amount of data needed to capture web content, and how often the webpage is updated.

### Technological Limitations

It may not be possible to save a complete archive of all web content due to technological limitations.  Requests are evaluated to determine if web content contains significant interactive elements, dynamic content (e.g. social media, ArcGIS), or robots.txt exclusions.  If these are present, the Libraries may not have the technological means, necessary data, or permission to capture the web content.

### Access  

Access to archived web collections is currently provided through Archive-It’s access [portal](https://archive-it.org/home/wustl). The Libraries are open to and actively exploring additional approaches to providing access and raising the profile of web collections.

### Preservation

  The Libraries rely on Archive-It's services as outlined in its storage and preservation policy to preserve and manage web archives.  The Libraries intends to preserves web archive files (WARC) in perpetuity according the digital preservation policy.  

### Privacy Policy

[Libraries Privacy Statement](https://library.wustl.edu/about/policies/privacy-statement/)

### Takedown Policy

The Washington University Libraries Web Archive collections preserve web pages as they change over time – it captures content that has been, or may be, removed from the live web. The objective is to document Washington University’s web presence. The primary focus is the university [domain](https://wustl.edu/) and all subdomains encompassing the activities of the administration, faculty, students, and staff.  If there are questions or concerns about the web content, please contact the Julian Edison Department of Special Collections, spec@wumail.wustl.edu.  

## Responsibilities

___

| Position or Office | Responsibilities |
| ------------------ | ---------------- |
| Special Collections Management Department | Includes curatorial and processing staff who oversee, manage, and contribute to the web archive, including activities related to the appraisal, acquisition, and access to web content.  |
| Preservation Unit | Oversees and manages the Libraries’ digital preservation strategies. |
| Digital Processing Archivist | Oversees and maintains the documentation, workflows, and processes related to web archiving.  These processes include the appraisal, acquisition, and access to web content.  Primary contact for the web archiving program.
| Born-Digital Processing Assistants (2) | Support the ongoing development and maintenance of web archive collections by crawling, reviewing, and evaluating web captures. |  
| Washington University Archives | Primary stakeholder in the development of the web archive collection, including oversight of activities related to the curation, acquisition, and description web content. |
| Repository Services Manager | Manages the repository where web archive files may be held. |
| Library Technology Services Department | Manages the technical infrastructure needed to create, ingest, store, transform, and provide access to digital resources. Creates, installs, and maintains software as needed and provides support for staff using these tools. |

## Resources

___

- Washington University in St. Louis Archive-It [Homepage](https://archive-it.org/home/wustl)

- [Materials Use Policy](https://library.wustl.edu/wp-content/uploads/2015/11/spec-form02-materials-use-1.pdf)

- [Archive-It Storage and Preservation Policy](https://support.archive-it.org/hc/en-us/articles/208117536-Archive-It-Storage-and-Preservation-Policy)  

- [Digital Preservation Policy](TBD)

## Contacts

___

**Robert Manley**, Digital Processing Archivist, rmanley at wustl.edu

**Sonya Rooney**, University Archivist

- Draft date: 01-25-2022

- Revision date: 03-04-2022
