
# Kryoflux GUI Workflow for Floppy Disks

Creating disk images of (most) floppy disks with the GUI. :)

## Purpose and Scope

This workflow outlines the use of the Kryoflux GUI to create a disk image of the most common floppy disks which require a MFM sector image (80 & 40 track) or Apple DOS 400/800 sector image . It applies to the following disks: 

| MFM Sector Image (80 Track) | MFM Sector Image (40 Track) | Apple DOS 400/800 Sector Image |
| ------ | ------ | ------ |
| 3.5" **HD** PC/MAC | 5.25" **DD** Kaypro | 3.5" DD MAC |
| 3.5" **DD** PC | 5.25" **DD** PC |
| 5.25" **HD** PC |


## Identifying Disks and Filesystems

This workflow assumes that the disk-type and filesystem can be identified with a certain level of confidence. This is done by observing physical characteristics, understanding the manufacturers label, and gathering information about the files the user may have described on the label.

**Physical Characteristics**

- 5.25" disks are larger and have the characteristic "flop". They were commonly used in the 1980's and were a precursor to the 3.5" floppies. 
- 3.5" disks are smaller and are made of rigid plastic with a metal shutter. A single rectangular hole means the disk is double-density (720kb) and likely older than high-denisty (1.44mb) disks with 2 holes. 1 hole has a slideable "writeblocker" which should be in the "open" position prior to imaging.

**Manufacturers Labels** 

- 5.25" disks have labels indicating the density, which can be represented with letters (MD2-HD, 2S/2D) or written out (double-sided, high-density, double-sided/double-density]. In some cases, the capacity (360kb, 500kb) and computer system the disk was formatted for (IBM PC's) will be in fine print. The latter being especially useful in determining the filesystem. 
- 3.5" disks also have labels indicating the density (HD, DD) and can have the capacity and format (PC, Macintosh) on the label.
- **5.25" 40 track disks**: In addition to identifying a disk as double-density, 40 track disks can be confirmed by looking for labels that say "48 tpi" or "40 tracks", and/or have a manufacturing date prior to the mid-1980s, when they were the common format.

**User Labels**

- For both 5.25" and 3.5" disks, the user label can be useful in identifying the filesystem if it isn't printed on the manufacturer's label. Look for programs (word), file extensions (.doc) and other indications of the filesystem. 
- **3.5" DD MAC**:The manufacturer and user labels are especially important when identifying 3.5" DD disks. If they are MAC formatted, they will require a different disk-image format.

**Can't Identify a Disk?**

- The color-coded GUI output will also provide information to determine disk-type and filesystem. If a 5.25" disk has alternating good/bad sectors then it is a 40 track, and if a 3.5" DD disk has all gray sectors then there is a good chance it is MAC formatted. If the disk is still unidentified, it is either completely inaccessible or one of the *other* formats compatible with the Kryoflux.  


## Note on Handling

The Kryoflux board is fragile and vulnerable to changes in the electrical current. Follow these precautionary measures to protect it.

1. Handle it as little as possible to avoid damage.
2. When not in use store it in the custom box.
3. Plug the power supply into a surge protector or FRED.
4. Keep it away from conductive materials, such as metal tabletops or other potential sources of static electricity.
5. While in use place it on top of something non-conductive, such as an anti-static mat (the white mat on the workstation desk).
6. **Always** follow the steps to connect and disconnect it.

## Getting Started

These are the things that should be in place before beginning to disk image.

- Disks inventoried on media-log.
- Identify appropriate workflows and disk-image formats.
- Write-protect tabs open on 3.5" HD floppy disks.
- Output directory created (to save disk-images).
- Local server location identified for backup with same directory structure as the output directory.
- Know the proper handling of the Kryoflux.

## Basic Workflow

These are the basic steps to follow during each session of disk-imaging.

- Connect the Kryoflux (Power on FRED)
- Open the Kryoflux GUI and:
    - Calibrate disk drive
    - Confirm output directory and logs is checked
    - Confirm appropriate disk-image type and settings
- Open the media-log
- Pull out the next disk in the queue
- Confirm that the floppy disk mediaID and media label match the record in the media-log
- If applicable, confirm the write-blocker tab is open
- Insert disk
- Enter mediaID into the "Enter Name" field
- Start 
  - It's useful to have the output direcorty open to visually confirm the disk-image and log are being saved there.
  - **Do not stop the disk-imaging process before it is complete**.
- When complete, record actions in the media-log
  - Update status (imaged, imaged w/ errors, errors, inaccessible)
  - Add outcome of disk-imaging process
  - Highlight notes field red if there are bad sectors
  - Record date and intials
  - If the outcome does not conform to typical results for the disk-image type (all sectors gray, alternating colored sectors) highlight the row and set the disk aside for troubleshooting. 
- Eject 
- Repeat steps with next disk

## Connect the Kryoflux

Prior to connecting, gather the necessary componenets: disk drive (3.5"; 5.25"), Kryoflux, power cable, and data cable. Consult the troubleshooting entry [understanding drives 1 and 0](#understanding-drives-1-and-0) for an overview of data cable and for help identfying "drive 0" and "drive 1" for the 3.5" and 5.25" drives. (Note: I have found that is best to start with "drive 0", and if it doesn't work, switch to "drive 1" until the dive is calibrated.)

1. Connect the data cable to the Kryoflux and disk drive. 

- **Note**: The long end with a single connector plugs into the Kryoflux, and the other end with 2 connectors plugs into the disk drive. Additionally, confirm the red line is aligned with pin 1. A tiny arrow will be visible on the plastic casing of the Kryoflux and metal housing of the 3.5" drive. The 5.25" drive has a small white #1 next to the pin.

*The following images use 3.5" floppy drive to illustrate the workflow. The 5.25" floppy drive follows the same steps but require the other set of data and power cable connectors.* 

![connect-step-1](../images/kryoflux-step1-b.png) 

![connect-step-1](../images/kryoflux-step1-a.png)

___

2. Connect the USB cable **first** to the Kryoflux, **then** to the computer.

![connect-step-2](../images/kryoflux-step2-a.png "Connect to Kryoflux first")

![connect-step-2](.../images/kryoflux-step2-b.png "Connect to computer second")

___

3. Connect the power supply from the floppy drive to FRED. Note: Power on the writeblocker after connecting both power supplies.

![power-step-3](../images/kryoflux-step3-a.png)

![power-computer-step-3](../images/kryoflux-step3-b.png)


## Disconnect the Kryoflux 

Note: reverse the connection instructions :)

1. Power off writeblocker.
2. Disconnect the floppy drive’s power supply.
3. Eject and disconnect the USB cable.
4. Disconnect the data cable/floppy drive cable.

## Capture Disk Images with the GUI

1. Launch the Kryoflux graphical user interface, or GUI, by holding command+shift and searching for Kryoflux UI.  Select the application to open it.

2. At the beginning of each imaging session calibrate the floppy drive. The drive should only need to be calibrated once per imaging session unless the drive is switched between 3.5-inch and 5.25-inch.
    - Select the correct drive from the drive menu. This may require some trail and error.
    - Select calibrate.

![calibrate](../images/kryoflux-gui-calibrate.png)

3. Select the output directory to save disk images and log files.
- Select *File → Settings* 
- In settings, select the *Output* tab.
- Browse to the appropriate path in storage (see setting up directory structure here...tbd).  
- Ensure that the *Logs* button is checked.
- Click *OK*.

![set-output](../images/kryoflux-gui-output.png)

4. Enter unique identifier and select image format.
- Click on *Enter name…* and type in the mediaID associated with the disk.
    - The text entered here will become the filename for any disk images and log files created.  Do not include the extension in the file name.
    - Confirm that the floppy disk mediaID and media label match the record in the media-log.
- Select the image format(s) for the disk image using the dropdown list below the filename field (see figure 12).
- Consult the table below (figure 11) to  select the necessary image formats.
- In order to choose multiple outputs, hold down the *Control (Ctrl)* key while making your selections.
- In most cases, selecting an image format to obtain a sector image requires that you know something about the media in hand. I.e.: It is a PC or MAC formatted 3.5 inch floppy disk.

![select-format](/images/kryoflux-gui-format.png)

|  **Physical Format** | **System Format** |     **Kryoflux Image Format**    |
|:--------------------:|:-----------------:|:--------------------------------:|
| *3.5" double density  | Macintosh         | Apple DOS 400K/800K sector image* |
| 3.5" double density  | PC                | MFM sector image                 |
| 3.5" high density    | Any               | MFM sector image                 |
| **5.25" double density | Kaypro            | MFM sector image (40 track)**      |
| **5.25" double density | PC                | MFM sector image (40 track)**      |
| 5.25" high density   | PC                | MFM sector image                 |

Figure : Some of the most commonly used disk encoding formats supported by the Kryoflux.

5. Image the disk.
- After selecting the appropriate image format, insert a disk and select *Start*. 
  - For 3.5" floppies double check to confirm that the writeblock "tab" is open.
- The green *Stream* indicator should flash on and off, and the cells in the *Tracks* display on the left-hand side of the window should fill with different colors and, in some cases, letters. The colors and pattern of the track cells have specific meanings which will determine if troubleshooting is necessary.

| **Color** | **Meaning** | **Action** | **Image** |
| :----------------------: | ----------- | ------ | ----- |
| Green | *Good:* The track was imaged successfully. | None | ![Green Modified](../images/kryoflux_track_modified_2.png) |
| Orange| *Good+Modified:* The track was imaged successfully, but has one or more sectors that were modified after formatting or mastering. | This is normal for disks that have been used over and over again. | ![Orange Modified](../images/kryoflux_track_modified_2.png) |
| Red | *Bad:* The track was not imaged successfully. | Highlight the disk red in the medialog and set aside for troubleshooting. | ![Bad Sectors](../images/kryoflux_track_bad_sector.png) |
| Grey | *Unknown:* the Kryoflux software could not determine the status of this track. It could indicate that this track was unformatted or that the wrong format was selected at step 5. | If it is 3.5" DD MAC, change the format to Apple DOS 400/800 sector image. If not, highlight medialog and set aside for troubleshooting | ![Unknown Sectors](../images/kryoflux_track_gray_unknown.png) |
| Yellow | *Warnings:* Yellow blocks indicate a track is decoded with warnings. | Make a note in the medialog. | ![Yellow Sectors](../images/kryoflux_track_yellow.png) |
| Alternating Good and Unknown tracks | This is an indication that the disk may be 40 tracks. | Change the settings to 40 track and try again. | ![40 Track](../images/kryoflux_track_40.png) |

6. Once the disk stops spinning and the green *Stream* indicator stops flashing, the imaging process has completed. The disk image(s) and log file for the disk can be found in the directory selected in step 3.

7. Update media-log
- update status info:
  - imaged (if complete with no issues)
  - imaged with errors (successful but with a few bad sectors)
  - inaccessible (if the disk did not spin up and reading it was not possible)
  - error (if imaged with significant errors and bad tracks)
  - troubleshoot (if the disk needs to follow a different workflow)
- add notes about accessioning in *accessioning log*
- log Kryoflux output in *output*
- add highlights to rows for disks that need traoubleshooting
- log accession info:
  - date mm-dd-yyyy
  - examiner (initials)
  - format (mfm sector image)
  - interface (/images/kryoflux)
   
9. To image another disk, return to step 4. 

**Note**: When switching drives (e.g. from imaging 3.5” disks to 5.25") continue from step 3 to calibrate the drive.

## Troubleshooting

<img align="right" src="../images/drive1-0_figure01.png"> 

### Understanding Drives 1 and 0

Each board comes with a floppy data cable which has two sections, each section has two connectors (one for each drive type, 3.5 and 5.25 inch).  The section that is connected to the drive determines if that drive will appear as Drive 1 or Drive 0 in the GUI.  The connectors closest to the Kryoflux board will usually appear as Drive 1 and the connectors furthest from the board appear as Drive 0. For advanced use, it is possible to set the jumper on the board itself to support a dual drive setup (see the official Kryoflux manual). 

When connecting the data cable to the drive you can only connect one drive to one section at a time.  If you are connecting two drives to a Kryoflux board, be sure that you use one connector from each section of the data cable.  Make sure you have documented which drive is associated with which section.  This will be important to know in order to calibrate the correct drive for use. The order of which drive is associated with which drive number (1 or 0) does not affect the usage of the Kryoflux, but it must be documented in order to know which drive to calibrate.



## Resources

- [CCA](https://github.com/CCA-Public/digital-archives-manual/blob/master/guides/diskimaging.md/#Kryoflux)

- [NYPL](https://nypl.github.io/digarch/tools/Kryoflux)

- [Archivists Guide to the Kryoflux](https://wustl.box.com/s/h7b175t6ksgc4s6ezukhaa122zlxancg)

- ![Know your media guide](/images/KnowYourMedia_Poster_web_-_KnowYourMedia_Poster_web.pdf)