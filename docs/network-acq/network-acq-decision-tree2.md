# Network Acquisition Flowcharts

## Identify a Donor Type

When a new digital acquisition is ready to transfer to the archive, the first step is to determine the donor type.

## Identify a Location

The next step is to identify the location of the digital content to determine the method of acquisition.  

## Follow a Workflow

With the donor type and location in hand, follow a network acquisition workflow.

### Internal Donor Flowchart

![Internal Flowchart](network-acq-internal-donor-transparent.svg)

### External Donor Flowchart

![External Flowchart](network-acq-external-donor-transparent.svg)

## Finalize Acquisition

When the transfer is complete, finalize the acquisition by  visually confirming that the transfer is successful and the expected content is received.
