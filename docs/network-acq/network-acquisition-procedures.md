# Network Acquisition Procedures

Scope: The following outlines acquisition procedures for digital content that will be transferred to Special Collections via WUSTL Box or from a cloud storage platform.  This includes:

- Content located on Box.
- Locally stored content (e.g. desktop) that will be uploaded to WUSTL box.
- Content on other cloud management platforms (e.g. Google, Amazon, Dropbox) that will be transferred to the archive.

## WUSTL Box Network Transfers

WUSTL Box is the primary cloud storage provider for the university. Special Collections uses it to receive transfer of digital content acquired by archival collecting units.  It supports Box to Box transfers (e.g. copy and paste), transfers from local storage to Box, but it does not support direct transfers from other cloud service providers. It can be used to receive content from WUSTL affiliated donors from across the university community and from donors outside the university community.

Three basic steps to complete an acquisition via a network transfer are as follows:

### 1. Prepare

- Contact digital processing (via email, teams and other tbd) to coordinate the transfer and determine the best option.

- In the designated digital accessions folder on Box, create a collection level folder (if there isn't one) with the Collection Number and Name (Ex: wua00555-generic-coll).

- Inside the collection level folder, create another accession level folder with a short filename the donor will recognize. This folder will be what they see in the "all files" view in their Box account.  Upon completion of the transfer it will be changed to collection number & accession number (wua00555_wua2022-001).

- For more complex transfers involving a number of donors and collaborators (e.g. class projects) other organizational strategies can be determined in coordination with digital processing.

### 2. Transfer

#### Internal Donors

For university affiliated donors with a WUSTL Box account follow these steps:

- Add the donor to the accession level folder.

- Apply the appropriate donor permissions:

  : **Editor**: Allows the donor to delete, rename or otherwise manage folders/files after upload (encourage them to do this prior to uploading).
  : **Viewer Uploader**: Allows the donor to upload, download, edit, and share folders/files.
  : **Previewer Uploader**: Allows donor to upload and preview (ideal if multiple donors are uploading to the same folder).

- Create and share the accession level folder link with the donor and let them know the box location is ready to receive their transfer.

- For local acquisitions ( e.g. from a desktop), the donor can upload files and folders using the [web browser](https://support.box.com/hc/en-us/articles/360044196633-Upload-to-Box-With-the-File-Browser) or Box [Drive](https://www.box.com/resources/downloads).

- For acquisitions already located in Box, the donor can use the [copy utility](https://support.box.com/hc/en-us/articles/360044196593-Moving-and-Copying-Files-Folders) located under the *more options* menu.


#### External Donors

For donors not affiliated with the university there are three options to add content to WUSTL Box:

- The donor can create a free box account and then follow the instructions for affiliated WUSTL Box transfers (above). This is a preferred route if the donor has a stable internet connection, sufficient technical understanding of cloud platforms,  and large folders/significant number of files to transfer to the archive.

- For some small acquisitions [enabling email uploads for folders](https://support.box.com/hc/en-us/articles/360043697534-Upload-to-Box-Through-Email) will allow the donor to email folders and files to Box without an account.  To do this, enable email uploads for the accession level folder, copy the address and email it to the donor. The upload size will be limited by the email client's attachment size specifications (e.g.: Gmail, 25mb; Outlook, 20mb).  If transferring a folder, it may need to be compressed with a ZIP utility. The donor will receive an email indicating if the transfer was successful.

- For a small number of individual files enable the [File Request Form](https://support.box.com/hc/en-us/articles/360045304813-Using-File-Request-to-get-Content-from-Anyone).  Share the file request link for the accession level folder with the donor, and they will be able to upload files to Box without an account. Folders **cannot** be uploaded with the form.  Individual file size upload limit is determined by Box.

Note: Methods 2 and 3 can also be used with affiliated donors who have small one-off acquisitions.

### 3. Finalize

To finalize the acquisition the curator will:

- Communicate with the donor and visually confirm that the transfer is successful and expected content is there.

- Remove donor's permissions from the Box accession level folder.

- Fill out and submit the accession form.

- Notify digital processing that the transfer is ready.

___

## Other Cloud Network Transfers

The methods and requirements for acquiring content depends on the cloud service the donor uses.  These services can include Google Drive, Dropbox, AWS, and Apple iCloud. At this time, transferring files between two different cloud service providers is not possible. Despite the obstacles, it is possible to outline two basic routes.  In either case, digital processing should be consulted to determine the best route.

1. The donor can **download** the acquisition from their cloud service provider to their local workstation, and then upload it to WUSTL Box following the affiliated donor instructions (above).

2. The donor can **share** a link with digital processing staff (digacqstud@gmail.com), which allows staff to download the acquisition directly to the digital processing workstation for accessioning.

Note: There may be additional ways to transfer content from a cloud storage provider, but they will need to be evaluated on a case by case basis.

## Additional Transfer Methods

If the above acquisition methods will not work, it is possible to determine other methods to transfer content to the archives.  Here are three options:

1. A [FTP](https://support.box.com/hc/en-us/articles/360043697414-Using-Box-with-FTP-or-FTPS) service can be used to manage large transfers requiring bulk uploading and downloading.  This requires additional setup and configurations which should be done in consult with digital processing.  There are a number of tools available including [Filezilla](https://filezilla-project.org/) and [Exactly](https://www.weareavp.com/products/exactly/).  This option only works to transfer files to and from cloud storage to local locations.

2. As an alternative to network transfers, an external hard drive or portable media device (e.g. usb drive) can be used to transfer acquisitions.  This is a good option if it is a large quantity of content stored locally.  

3. For acquisitions located on a local WUSTL Server (e.g.: photo services), it is possible to work with IT to gain access and transfer files directly to WU Library managed server locations.
